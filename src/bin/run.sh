#!/bin/sh
# PoweredBY: 子秋(ziquee QQ:39886616) http://www.jdiy.club
#
APPNAME="demo"
PORT=8886
APPROOT="/data/${APPNAME}"

kill $(ps aux | grep ${APPNAME}.jar | grep -v 'grep'  | tr -s ' '| cut -d ' ' -f 2)

if [ -f "${PORT}.log" ]; then
  rm ${PORT}.log
fi

if [ -f "${APPROOT}/${APPNAME}.jar" ]; then
  rm ${APPROOT}/${APPNAME}.jar
fi

cp ${APPROOT}/${APPNAME}-5.0-SNAPSHOT.jar ${APPROOT}/${APPNAME}.jar
nohup java -jar ${APPROOT}/${APPNAME}.jar --server.port=${PORT} --spring.profiles.active=prod > ${APPROOT}/${PORT}.log &
