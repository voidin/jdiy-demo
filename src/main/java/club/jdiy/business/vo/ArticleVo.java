package club.jdiy.business.vo;

import club.jdiy.business.entity.Article;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 一个VO
 * VO是可以安全返回到客户端的普通POJO类。
 * 它的数据一般由Entity实体类赋值转换而来，做了一些简化(剔出客户端不需要的字段)/或数据格式化/或数据脱敏处理等。
 */
@Data
public class ArticleVo implements Serializable {
    private Integer id;
    private String title;
    private String hits;
    private LocalDateTime createTime;

    public ArticleVo(Article po) {
        this.id = po.getId();
        this.title = po.getTitle();
        this.createTime = po.getCreateTime();
    }
}
