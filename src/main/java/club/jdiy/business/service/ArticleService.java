package club.jdiy.business.service;

import club.jdiy.core.base.JDiyService;
import club.jdiy.business.dao.ArticleDao;
import club.jdiy.business.entity.Article;

/**
* 文章 Service接口层        (这只是一个演示。主要是编码开发用到。  通过在线开发生成的界面不需要这个东西)
 */
public interface ArticleService
        extends JDiyService<Article, ArticleDao, Integer> {
    //注意Service层接口的 extends后面的内容，固定写法，JDiyService后面第1个泛型是实体类，第2个是该实体的Dao接口, 第3个是实体的主键类型
    //实现层就把JDiyService改成JDiyBaseService, 详见ArticleServiceImpl

    //业务接口，方法创建(常用的JPA增删改查方法，都不需要写。JDiy已内置。这只是演示随便写的一个方法)
    void foobar(Article article);
}
