package club.jdiy.business.service.impl;

import club.jdiy.business.dao.CategoryDao;
import club.jdiy.business.entity.Category;
import club.jdiy.business.service.CategoryService;
import club.jdiy.core.base.JDiyBaseService;
import org.springframework.stereotype.Service;

//[文章栏目] service实现层。     (这只是一个演示。主要是编码开发用到。  通过在线开发生成的界面不需要这个东西)
// 一个空壳（与Dao一样，同样集成了完善的增删改查内置方法）。注意extends/implements后面的内容。
@Service
public class CategoryServiceImpl
        extends JDiyBaseService<Category, CategoryDao, Integer>
        implements CategoryService {

    //业务方法实现类。
    //@Override
    //public void foobar(Article article) {
    //.................    业务代码略
    //当前实体的dao直接使用：
    //dao.save(article);
    //..................   让您专注于编写业务逻辑
    //}

    //注入其它实体的dao:
    //@Resource
    //private UserDao userDao;
    //...
}
