package club.jdiy.business.entity;

import club.jdiy.core.base.domain.JpaEntity;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * 会员实体 (for: JDiy演示专用 )      (这只是一个演示。主要是编码开发用到。  通过在线开发生成的界面不需要这个东西. )
 */
@Data
@Entity
@Table(name = "demo_vip")
public class Vip implements JpaEntity<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;//主键(此处定义为数据库自增)

    @Column(length = 30)
    private String username;//用户名
    @Column(length = 32)
    private String password;//密码

    @ManyToOne
    private VipLevel level;//会员等级
    @ManyToOne
    private Area area;//会员所在地区

    @Column(length = 20)
    private String name;//会员姓名
    @Column(columnDefinition = "int default 0 not null comment '姓别：1:男 2:女  0:未知'")
    private Integer gender;//姓别：1:男 2:女  0:未知
    private LocalDate birthday;//出生日期
    @Column(length = 18)
    private String sfz;//身份证号
    @Column(length = 20)
    private String phone;//会员的手机号
    @Column(length = 50)
    private String email;//会员邮箱
    @Column(length = 64)
    private String addr;//会员用户地址

    private String hobby;//兴趣爱好（演示多选框，本表直存, 多个爱好逗号分隔）

    private String avatar;//用户头像（演示本表直存附件路径） 注：附件可以使用JDiyStore对象存储，业务数据表里面不建字段记录附件路径也可以

    @Column(columnDefinition = "int default 0 not null comment '是否停用: 0:否 1:是'")
    private Boolean disabled;//是否停用(注：boolean值在mysql中定义为 bit(1)类型，在JDiyAdmin后台中用0,1表示)
    @Column(columnDefinition = "text")
    private String remark;//用户备注

}
