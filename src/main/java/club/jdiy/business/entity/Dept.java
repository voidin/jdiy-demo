package club.jdiy.business.entity;

import club.jdiy.core.base.domain.JpaEntity;
import club.jdiy.core.base.domain.Sortable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * 部门信息(又是一个树结构)
 */
@Data
@Entity
@Table(name = "b_dept")
@NoArgsConstructor
public class Dept implements Sortable, JpaEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(columnDefinition = "varchar(50) not null comment '部门名称'")
    private String name;

    @Column(columnDefinition = "int not null default 100 comment '排序索引(数字越小越靠前)'")
    private Integer sortIndex;

    @ManyToOne
    private Dept parent;//上级部门

    @OneToMany(mappedBy = "parent")
    @OrderBy("sortIndex ASC")
    private List<Dept> children;//子级部门

    @Column(columnDefinition = "text not null comment '树结构的搜索路径'")
    private String sortPath;


    public Dept(Long id) {
        this.id = id;
    }


    /**
     * 实体持久化事件，从编码层面设置字段的默认值。
     */
    @PreUpdate
    @PrePersist
    private void init() {
        if (sortIndex == null) sortIndex = 100;
    }
}
