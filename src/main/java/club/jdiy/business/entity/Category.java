package club.jdiy.business.entity;

import club.jdiy.core.base.domain.JpaEntity;
import club.jdiy.core.base.domain.Sortable;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 简结的文章栏目(演示文章和栏目的多对一/一对多关系)
 */
@Data
@Entity
@Table(name = "b_category")
public class Category implements JpaEntity<Integer>, Sortable {
    @Id
    private Integer id;
    private String name;//文章栏目
    private Integer sortIndex;//排序索引
}
