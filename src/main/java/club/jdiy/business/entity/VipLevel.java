package club.jdiy.business.entity;

import club.jdiy.core.base.domain.JpaEntity;
import club.jdiy.core.base.domain.Sortable;
import lombok.Data;

import javax.persistence.*;

/**
 * 会员等级
 * <p>
 * (for: JDiy演示专用 )
 * <p>
 * 一个会员有一个等级属性 (多对一)；
 * 一个等级下面可以有多个会员（一对多）
 */
@Data
@Entity
@Table(name = "demo_vip_level")
public class VipLevel implements JpaEntity<Integer>, Sortable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;//主键(此处定义为数据库自增)

    @Column(length = 50)
    private String levelName;//等级名称

    @Column(columnDefinition = "int default 100 not null comment '排序数字（越小越靠前）'")
    private Integer sortIndex;
}
