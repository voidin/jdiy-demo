package club.jdiy.business.entity;

import club.jdiy.core.base.DslFilter;
import club.jdiy.core.base.domain.DslFilterable;
import club.jdiy.core.base.domain.JpaEntity;
import club.jdiy.utils.StringUtils;
import com.querydsl.core.BooleanBuilder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 文章实体（仅演示）
 */
@Data
@Entity
@Table(name = "b_article")
public class Article implements JpaEntity<Integer>, DslFilterable {
    @Id
    private Integer id;
    @ManyToOne
    private Category category;//文章所属栏目

    private String title;//文章标题
    @Column(length = 32)
    private String author;//文章作者
    private LocalDateTime createTime;//创建时间
    @Column(columnDefinition = "longtext comment '文章内容'")
    private String content;
    private Integer hits;//点击次数

    //非持久化字段。这个主要是为了使用动态查询（按创建时间：起止时段查询，这个字段为结束时段）
    @Transient
    private LocalDateTime createTime_;

    //将实体类implements于 DslFilterable,即表明开启queryDSL动态查询支持。
    //开启后，写上这个方法，根据业务需要增加动态查询条件
    //然后Article此实体对应的Dao/Service层中，就可以使用诸如  findList, findPager等动态查询方法了。
    //动态查询主要用于信息分页列表展示页面。顶部可以增加N多个搜索条件（用户可以输入也可以不输入）输入了对应的条件，就增加该字段的查询过滤。
    //controller中相应动态查询调用示例：
    //  Article qo =new Article();
    //  qo.setTitle("标题关键字");
    //  Pager<Article> pager = service.findPager(pageSize,page, qo);
    //  .....查询出来了。做相应view层数据展示
    //
    @Override
    public DslFilter createFilter(BooleanBuilder builder) {
        QArticle qo = QArticle.article;

        if (StringUtils.hasText(title)) builder.and(qo.title.contains(title.trim()));//按标题关键字查询
        if (category != null) {
            if (category.getId() != null) builder.and(qo.category.eq(category));//按文章分型查询
        }
        if (createTime != null) builder.and(qo.createTime.goe(createTime));//按时段查询(起始时间)
        if (createTime_ != null) builder.and(qo.createTime.loe(createTime_));//按时段查询(结束时间)

        //todo .... 自行增加其它动态查询条件
        return new DslFilter(builder, qo.createTime.desc());
        //DslFilter第2个参数为默认排序。即当findPager, findList等动态查询条件没有指定排序规则的时候，使用此默认排序。
    }
}
