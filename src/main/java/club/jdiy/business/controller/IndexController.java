package club.jdiy.business.controller;

import club.jdiy.business.vo.ArticleVo;
import club.jdiy.core.base.JDiyCtrl;
import club.jdiy.core.base.domain.Pager;
import club.jdiy.core.base.domain.Ret;
import club.jdiy.business.entity.Article;
import club.jdiy.business.service.ArticleService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

/**
 * 这只是一个前台的Controller示例。
 */
@Controller
public class IndexController
        extends JDiyCtrl<Article, ArticleService> {
    //注意上面这行，extends后面的内容。所有的controller都可以是这种写法，不同的controller就把Article换成对应的实体类。
    //注意后台管理是extends JDiyAdminCtrl，这儿不是后台控制器，就是JDiyCtrl



    //首页。 此处直接重定向，跳到后台登录页面去了：
    @RequestMapping({"", "/"})
    @ResponseBody
    public String index(HttpServletResponse response) {
        //重定向
        return Ret.direct(response, Ret.Type.url, context.getContextPathURL() + "/mgmt/");
    }


    //普通页面显示：
    @RequestMapping("sayHello")
    public String sayHello(ModelMap map, Integer id) {
        //todo something
        service.findById(id).ifPresent(article -> {
            map.put("article", article);
        });
        map.put("myName", "JDiy");
        return "hello.ftl";
    }

    //ajax请求/响应：
    @RequestMapping("doAjax")
    public Ret<ArticleVo> sayHello(@RequestParam(defaultValue = "1") Integer id) {
        try {
            //todo something
            //这只是一个演示，ajax请求的controller写法。实际就是根据id显示文章概要（标题/id/添加时间）
            Article article = service.findById(id).orElse(null);

            if (article == null) return Ret.fail("没有找到文章。");//普通fail提示信息json返回

            else return Ret.success(new ArticleVo(article));//转成一个vo返回了  正常数据返回

        } catch (Exception ex) {
            return Ret.error(ex);//错误信息返回
        }
    }


    //演示ajax请求返回一个带分页的list数据：
    @RequestMapping("getArticles")
    public Ret<Pager<ArticleVo>> pager_demo(Article qo, //注意这就是动态查询的条件Model(直接用的实体类。当然您也可以自建其它查询qo对象)
                                            @RequestParam(defaultValue = "10") Integer pageSize,
                                            @RequestParam(defaultValue = "1") Integer page
    ) {
        try {
            Pager<Article> pager = service.findPager(pageSize, page, qo);

            return Ret.success(pager.to(ArticleVo::new));//一键将Article实体分页对象转成一个Vo分页对象
            //上行使用了lambda表达式一键转换,原生写法如下：
            /*
            List<ArticleVo> list= new ArrayList<>();
            for(Article a:pager.getItems()){
                list.add(new ArticleVo(a));
            }
            return Ret.success(new Pager<>(page,pageSize,pager.getRowCount(),list));
            */


        } catch (Exception ex) {
            return Ret.error(ex);//错误信息返回
        }
    }

}
