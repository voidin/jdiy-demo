package club.jdiy.business;

import club.jdiy.core.base.domain.ConfigBean;
import lombok.Data;

import java.time.LocalDate;

/**
 * 系统全局配置信息。
 * 要在程序中获取配置信息,使用：
 * <p>
 * AppConfig cfg = context.getConfig(AppConfig.class);
 * 要修改配置信息，使用：
 * appContext.saveConfig(cfg);
 */
@Data
@SuppressWarnings("unused")
public class AppConfig implements ConfigBean {
    private Boolean tjTestOn;//测试boolean

    private String someConf = "test";//测试２.

    private Integer testInt = 12345;//整数
    private LocalDate testDate;//日期 (还可以是LocalDateTime，日期和时间， java.util.Date都行。)

    //理论上说全局变量尽量不要使用基本数据类型（int,boolean,double,long）,而要使用其对象类型。
    //如果要增加一个全局变量，要设置其默认值，就直接给它定义初始值.

}
