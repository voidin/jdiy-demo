<#include "/templates/common/jdiy_macros.ftl"/>
<#assign inWh=[620,440]/>
<blockquote class="layui-elem-quote" style="color: #c00;background-color: #fff">
    这是一个自已代码编写的增删改查演示(非在线开发生成).演示如何自定义页面完成基本的增删改查
</blockquote>
<@body>
    <@shForm>
        <div class="layui-form-item" style="margin-bottom: 0;">
            <div class="layui-inline">
                <label class="layui-form-mid">启用状态</label>
                <div class="layui-input-inline" style="width:120px;">
                    <select name="enabled" lay-search="">
                        <option value="" selected="">=全部=</option>
                        <option value="true" <#if qo.enabled?? && qo.enabled>selected</#if>>启用</option>
                        <option value="false" <#if qo.enabled?? && !qo.enabled>selected</#if>>未启用</option>
                    </select>
                </div>
            </div>
            <div class="layui-inline" style="margin-right: 0;">
                <label class="layui-form-mid">时期范围</label>
                <div class="layui-input-inline" style="width:100px;">
                    <input name="theDate" value="<#if qo.theDate??>${qo.theDate?date}</#if>" type="text"
                           class="layui-input fmt_date">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-mid">～</label>
                <div class="layui-input-inline" style="width:100px;">
                    <input name="theDate_" value="<#if qo.theDate_??>${qo.theDate_?date}</#if>" type="text"
                           class="layui-input fmt_date">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-mid">名称</label>
                <div class="layui-input-inline">
                    <input name="name" value="${qo.name}" type="text" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <button type="submit" class="layui-btn layui-btn-normal">检索</button>
            </div>
        </div>
    </@shForm>

    <@card>
        <@cardHead>
            <button class="layui-btn layui-btn-sm" href="${ctx}/mgmt/${modelName}/in" target="dialog"
                    width="${inWh[0]}" height="${inWh[1]}" title="添加">添加
            </button>
        </@cardHead>
        <@cardBody>
            <table lay-filter="table${_}" class="layui-table" <#--lay-data="{initSort:{field:'uid', type:'asc'}}"-->>
                <thead>
                <tr>
                    <th lay-data="{field:'name', align:'center'}" style="text-align: center">名称</th>
                    <th lay-data="{field:'theDate',align:'center'}" style="text-align: center">日期</th>
                    <th lay-data="{field:'amount',align:'center'}" style="text-align: center">金额</th>
                    <th lay-data="{field:'sortIndex',align:'center'}" style="text-align: center">排序</th>
                    <th lay-data="{field:'enabled',align:'center'}" style="text-align: center">状态</th>
                    <th lay-data="{field:'mge',width:180,align:'center'}" style="text-align: center">管理</th>
                </tr>
                </thead>
                <tbody>
                <#list pager.items as it>
                    <tr>
                        <td>${it.name}</td>
                        <td>${it.theDate}</td>
                        <td>${it.amount}</td>
                        <td>${it.sortIndex}</td>
                        <td>
                            <#if it.enabled>
                                <div style="color:green">启用</div>
                            <#else>
                                <div style="color:red">不启用</div>
                            </#if>
                        </td>
                        <td>
                            <#if grant(modelName+':edit')>
                                <a class="layui-btn layui-btn-xs dblclick"
                                   href="${ctx}/mgmt/${modelName}/in?id=${it.id}"
                                   target="dialog" width="${inWh[0]}" height="${inWh[1]}" title="修改">修改</a>
                            </#if>
                            <#if grant(modelName+':del')>
                                &nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs"
                                         href="${ctx}/mgmt/${modelName}/remove?id=${it.id}" target="ajaxTodo"
                                         confirm="您确定要删除此项吗？(警告：此操作不可恢复！)">删除</a>
                            </#if>
                        </td>
                    </tr>
                </#list>
                </tbody>
            </table>
            <div id="pager${_}"></div>
        </@cardBody>
    </@card>
</@body>
<@footer>
    <script>
        layui.use([], function () {

        });
    </script>
</@footer>
